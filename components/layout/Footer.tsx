import React from 'react';

const Footer: React.FC = () => {
    return (
        <footer className="bg-gray-800 text-white py-4 px-6 w-full text-center">
            Luis Miguel Fajardo Lagos - SMBS - 2024
        </footer>
    );
};

export default Footer;