import NoteList from '@/components/noteList/NoteList';
import { createClient } from '@/utils/supabase/server'
import Link from 'next/link';
import { redirect } from 'next/navigation';


export default async function Page() {
    const supabase = createClient();
    const { data: { user } } = await supabase.auth.getUser();

    if (!user) return redirect('/login');

    return (
        <div className="p-5 w-full">
            <Link href="/create" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg shadow-md mt-5">
                New Note
            </Link>
            <NoteList />
        </div>
    )
}