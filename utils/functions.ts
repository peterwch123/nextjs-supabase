'use client';

export function getOrigin() {
    let origin = '';
    if (typeof window !== 'undefined') {
        origin = window.location.origin;
    }
    return origin;
}
