'use client';
import React, { useEffect } from 'react';
import { useRouter } from 'next/navigation'
import NoteForm from '@/components/noteForm/NoteForm';
import { createClient } from '@/utils/supabase/client';
import { User } from '@supabase/supabase-js';

const CreateNotePage = () => {
    const [user, setUser] = React.useState<User>();
    const supabase = createClient();
    const { replace } = useRouter();

    useEffect(() => {
        verifyUser();
    }, []);

    const verifyUser = async () => {
        const { data: { user } } = await supabase.auth.getUser();
        user && setUser(user);
        if (!user) return replace('/login');
    }

    const handleSave = (title: string, content: string) => {
        fetch('/api/notes', {
            method: 'POST',
            body: JSON.stringify({ title, content, user_id: user?.id }),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(() => {
            replace('/');
        }).catch((error) => {
            console.error('Error:', error);
        });
    };

    return (
        <NoteForm
            formTitle="Create Note"
            onSave={handleSave}
        />
    );
};


export default CreateNotePage;