import DeployButton from "@/components/DeployButton";
import AuthButton from "@/components/AuthButton";
import { createClient } from "@/utils/supabase/server";
import FetchDataSteps from "@/components/tutorial/FetchDataSteps";
import Header from "@/components/Header";
import { redirect } from "next/navigation";

export default async function ProtectedPage() {
  const supabase = createClient();

  const {
    data: { user },
  } = await supabase.auth.getUser();

  if (!user) {
    return redirect("/login");
  }

  return (
    <div className="flex-1 w-full flex flex-col gap-20 items-center">
      <div className="w-full">
        <div className="py-6 font-bold bg-purple-950 text-center">
          This is a protected page that you can only see as an authenticated
          user
        </div>
        <nav className="w-full flex justify-center border-b border-b-foreground/10 h-16">
          <div className="w-full max-w-4xl flex justify-between items-center p-3 text-sm">
            {/* <DeployButton /> */}
            <AuthButton />
          </div>
        </nav>
      </div>

      <div className="animate-in flex-1 flex flex-col gap-20 opacity-0 max-w-4xl px-3 pb-8">
        <Header />
        <main className="flex-1 flex flex-col gap-6">
          <h2 className="font-bold text-4xl mb-4">Welcome</h2>
          <p>
            ¡Bienvenido! Te has registrado con éxito en nuestra aplicación de administración de notas. Esta aplicación, creada con Supabase y Next.js como práctica, está diseñada para hacer tu vida más fácil y organizada.
            Con nuestra aplicación, puedes crear, editar, eliminar y listar tus notas de manera eficiente. Ya sea que necesites tomar notas rápidas o guardar ideas importantes, nuestra aplicación está aquí para ayudarte.
            Disfruta de la simplicidad y la facilidad de uso de nuestra aplicación. ¡Estamos emocionados de verte poner tus ideas en acción!
          </p>
        </main>
      </div>
    </div>
  );
}
