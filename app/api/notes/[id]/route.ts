import { createClient } from "@/utils/supabase/server";
import { NextRequest, NextResponse } from "next/server";

type Context = {
    params: {
        id: number;
    };
};

export async function GET(req: NextRequest, context: Context) {
    const supabase = createClient()

    const { data, error } = await supabase
        .from('note')
        .select()
        .eq('id', context.params.id);

    if (error) {
        return NextResponse.error() // Remove the argument from the function call
    }

    const note = data ?? [{}];

    return NextResponse.json({ data: note[0] });
}

export async function PATCH(req: NextRequest, context: Context) {
    const { params: { id } } = context;
    const payload = await req.json();
    const supabase = createClient();

    if (!payload.title) {
        return NextResponse.json({
            status: 400,
            error: 'Title is required'
        });
    }

    const { data, error } = await supabase
        .from('note')
        .update(payload)
        .eq('id', id);

    if (error) {
        return NextResponse.error() // Remove the argument from the function call
    }

    return NextResponse.json({ data });
}

export async function DELETE(req: NextRequest, context: Context) {
    const { params: { id } } = context;
    const supabase = createClient();

    if (!id) {
        return NextResponse.json({
            status: 400,
            error: 'Id is required'
        });
    }

    const { data, error } = await supabase
        .from('note')
        .delete()
        .eq('id', id);

    if (error) {
        return NextResponse.error() // Remove the argument from the function call
    }

    return NextResponse.json({ data });
}